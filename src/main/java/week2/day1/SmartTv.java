package week2.day1;

public class SmartTv extends Television{
	public void connectwifi() {
		System.out.println("wifi connected");
}
	public void appinstall(String appname) {
		System.out.println(appname+" has been installed");
}
	public void IncreaseVolume(int i) {
		System.out.println("Increasing Volumn to" + i);
	}
	public static void main(String[] args) {
	SmartTv stv = new SmartTv();
	//change the syntax with the interface
	stv.Channelchange(2,"NDTV");
	stv.IncreaseVolume();	
	stv.VolumnMute();
	stv.sports();
	stv.news();
	stv.connectwifi();
	stv.appinstall("Youtube");
	stv.IncreaseVolume(1);
	}
}