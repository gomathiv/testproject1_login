package wdMethods;
import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import utils.ReadaExcel;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
public class ProjectMethods extends SeMethods{
	public String dataSheetName;
	@BeforeTest//(groups = "any")
	public void beforeTest() {
		System.out.println("@BeforeTest");
	}
	@BeforeClass//(groups = "any")
	public void beforeClass() {
		System.out.println("@BeforeClass");
	}
	@BeforeMethod//(groups = "any")
	@Parameters({"url"}) //Change to Parameter.tetng annotation
	
	public void login(String url/*,String Uname,String UPassword*/) throws InterruptedException {
		beforeMethod();
		startApp("chrome", url);
		//startApp("chrome", "http://leaftaps.com/opentaps");
		/*WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		//type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("password");
		type(elePassword, password);
		//type(elePassword, "crmsfa");
		Thread.sleep(3000);;
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement crm = locateElement("linktext", "CRM/SFA");
		click(crm);		*/
	}
	
	/*@AfterMethod//(groups = "any")
	public void closeApp() {
		closeBrowser();
	}*/ //Closed to run the Create lead testcase
	@AfterClass//(groups = "any")
	public void afterClass() {
		System.out.println("@AfterClass");
	}
	@AfterTest//(groups = "any")
	public void afterTest() {
		System.out.println("@AfterTest");
	}
	@AfterSuite//(groups = "any")
	public void afterSuite() {
		endResult();
	}
	
	@BeforeSuite//(groups = "any")
	public void beforeSuite() {
		startResult();
	}
  @DataProvider(name = "fetchData")
  	  public Object[][] fetchdata() throws IOException
		{
		Object[][] data = ReadaExcel.fetchdata(dataSheetName);
		return data;
  }
}






	
	/*	@BeforeMethod
		public void login() throws InterruptedException {
			//startApp("chrome", "https://www.zoomcar.com/chennai/");
			//Testleaf URL
			startApp("chrome", "http://leaftaps.com/opentaps");
			WebElement eleUserName = locateElement("id", "username");
			type(eleUserName, "DemoSalesManager");
			WebElement elePassword = locateElement("password");
			type(elePassword, "crmsfa");
			Thread.sleep(3000);
			WebElement eleLogin = locateElement("class","decorativeSubmit");
			click(eleLogin);	
			
		}
		@AfterMethod
		public void Closebrowser() {
			closeBrowser();
		}
		@AfterSuite
		public void closeAllBrowsers() {
			closeAllBrowsers();
		}
	} */