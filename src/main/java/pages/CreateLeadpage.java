package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadpage extends ProjectMethods{
 public CreateLeadpage() {
	 PageFactory.initElements(driver, this);
 }
 //Page Factory @Findby --> similar to your locate Element
 /*type(locateElement("id", "createLeadForm_companyName"), cName);
	type(locateElement("id", "createLeadForm_firstName"), fName);
	type(locateElement("id", "createLeadForm_lastName"), lName);
	type(locateElement("id", "createLeadForm_primaryEmail"), email);
	type(locateElement("id", "createLeadForm_primaryPhoneNumber"), ""+ph);
	click(locateElement("name", "submitButton"));*/
 @FindBy(how = How.ID ,using = "createLeadForm_companyName" ) WebElement eleCompanyname;
 @FindBy(how = How.ID ,using = "createLeadForm_firstName" ) WebElement elefirstname;
 @FindBy(how = How.ID ,using = "createLeadForm_lastName" ) WebElement elelastname;
 @FindBy(how = How.ID ,using = "createLeadForm_primaryEmail" ) WebElement eleprimarymail;
 @FindBy(how = How.ID ,using = "createLeadForm_primaryPhoneNumber" ) WebElement elephonenumber;
 @FindBy(how = How.CLASS_NAME ,using = "submitButton" ) WebElement elesubmitbutton;
 
 //Enter company name
 public CreateLeadpage enterCompanyname(String cName) {
	type(eleCompanyname,cName);
	return this;
 }
//Enter First name
public CreateLeadpage enterFirstname(String fName) {
	type(elefirstname,fName);
	return this;
}
//Enter last name
public CreateLeadpage enterlastname(String lName) {
	type(elelastname,lName);
	return this;
}
//Enter primary mail
public CreateLeadpage enterprimarymail(String pmail) {
	type(eleprimarymail,pmail);
	return this;
}
//Enter primary phone number
public CreateLeadpage enterprimaryphonenumber(String pnumber) {
	type(elephonenumber,pnumber);
	return this;
}
//Click Submit button
public ViewLeadpage ClickSubmit() {
	click(elesubmitbutton);
	return new ViewLeadpage();
}
 

}
