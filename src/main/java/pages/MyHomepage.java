package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyHomepage extends ProjectMethods{
 public MyHomepage() {
	 PageFactory.initElements(driver, this);
 }
 //Page Factory @Findby --> similar to your locate Element
 @FindBy(how = How.LINK_TEXT ,using = "Leads" ) WebElement eleleads;

 //click Leads tab
 public Myleadspage clickleads() {
		 click(eleleads);
		 Myleadspage mlp = new Myleadspage();
		 return mlp;
//		return new myHomepage;
 }

}

