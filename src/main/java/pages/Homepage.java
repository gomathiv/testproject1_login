package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class Homepage extends ProjectMethods{
 public Homepage() {
	 PageFactory.initElements(driver, this);
 }
 //Page Factory @Findby --> similar to your locate Element
 @FindBy(how = How.LINK_TEXT ,using = "CRM/SFA" ) WebElement eleCRMSFA;

 //Enter UserName
 public MyHomepage CRMSFAClick() {
		 click(eleCRMSFA);
		 MyHomepage mhp = new MyHomepage();
		 return mhp;
//		return new myHomepage;
 }

}

