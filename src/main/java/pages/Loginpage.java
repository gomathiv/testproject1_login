package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class Loginpage extends ProjectMethods{
 public Loginpage() {
	 PageFactory.initElements(driver, this);
 }
 //Page Factory @Findby --> similar to your locate Element
 @FindBy(how = How.ID ,using = "username" ) WebElement eleUserName;
 
 @FindBy(how = How.ID ,using = "password" ) WebElement elePassword;
 
 @FindBy(how = How.CLASS_NAME ,using = "decorativeSubmit" ) WebElement eleLogin;
 
 //Enter UserName
 public Loginpage enterUserName(String uName) {
	// WebElement eleUserName = locateElement("id","username");
	 type(eleUserName,uName);
	return this;
 }
//Enter UserName
public Loginpage enterPassword(String upassword) {
	// WebElement eleUserName = locateElement("id","username");
	 typeAndEnter(elePassword,upassword);
	return this;
}
//clickLogin
public Homepage ClickLogin() {
	// WebElement eleUserName = locateElement("id","username");
	 click(eleLogin);
	 Homepage hp = new Homepage();
	 return hp;
//	return new Homepage;
}
}
