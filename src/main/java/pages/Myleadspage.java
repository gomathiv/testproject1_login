package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class Myleadspage extends ProjectMethods{
 public Myleadspage() {
	 PageFactory.initElements(driver, this);
 }
 //Page Factory @Findby --> similar to your locate Element
 @FindBy(how = How.LINK_TEXT ,using = "Create Lead" ) WebElement elecreatelead;

 //click Leadstab
 public CreateLeadpage Createlead() {
		 click(elecreatelead);
		 CreateLeadpage mlp = new CreateLeadpage();
		 return mlp;
//		return new myHomepage;
 }

}

