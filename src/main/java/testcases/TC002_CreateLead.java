package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.CreateLeadpage;
import pages.Homepage;
import pages.Loginpage;
import pages.MyHomepage;
import pages.Myleadspage;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{
     @BeforeTest
     public void setdata() {
    	testCaseName = "Create Testlead";
 		testDesc = "Create Test Lead";
 		author = "Gomathi";
 		category = "smoke";
    	dataSheetName = "TC002";
    	   	 
     }
    
     public void crmsfa()
     {
    	 new Homepage()
    	 .CRMSFAClick();
     }
     public void leads()
     {
    	 new MyHomepage()
    	 .clickleads();
     }
     public void createleadbutton()
     {
    	 new Myleadspage()
    	 .Createlead();
     }
     @Test(dataProvider = "fetchData")
     public void createlead (String cName,String fName,String lName,String pmail,String pnumber)
     {
    	/* Loginpage lp = new Loginpage();
    	 lp.enterUserName("");
    	 lp.enterPassword("");*/
    	 
    	 
    	 
    	  new CreateLeadpage()
    	 .enterCompanyname(cName)
    	 .enterFirstname(fName)
    	 .enterlastname(lName)
    	 .enterprimarymail(pmail)
    	 .enterprimaryphonenumber(pnumber)
    	 .ClickSubmit();
     }
}
