package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.Loginpage;
import wdMethods.ProjectMethods;

public class TC001_LoginLogout extends ProjectMethods{
     @BeforeTest
     public void setdata() {
    	testCaseName = "Login Logout";
 		testDesc = "Login to Leaftap";
 		author = "Gomathi";
 		category = "smoke";
    	dataSheetName = "TC001";
    	   	 
     }
     @Test(dataProvider = "fetchData")
     public void loginlogout (String uName, String upassword)
     {
    	/* Loginpage lp = new Loginpage();
    	 lp.enterUserName("");
    	 lp.enterPassword("");*/
    	 new Loginpage()
    	 .enterUserName(uName)
    	 .enterPassword(upassword)
    	 .ClickLogin();
    	 
     }
}
