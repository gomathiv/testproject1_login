package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class ReadaExcel {

	@Test
	public static Object[][] fetchdata(String dataSheetName) throws IOException {
		// TODO Auto-generated method stub
    XSSFWorkbook wb = new XSSFWorkbook("./data/"+dataSheetName+".xlsx");
    XSSFSheet sheet = wb.getSheetAt(0);
    int rowNum = sheet.getLastRowNum();
    System.out.println(rowNum);
    short lastCellNum = sheet.getRow(0).getLastCellNum();
    System.out.println(lastCellNum);
    Object[][] data= new Object[rowNum][lastCellNum];
		for (int i = 1; i <= rowNum; i++) {
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j <=lastCellNum; j++) {
				XSSFCell cell = row.getCell(j);
				try {
					String CellValue = cell.getStringCellValue();
					System.out.println("Cell Values are" + CellValue);
					data[i-1][j]=CellValue;
				} catch (NullPointerException e) {
					// TODO Auto-generated catch block
					System.out.println("");
				}
			}
		}
		wb.close();
		return data;
	}

}
